@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="card mt-5">
              <div class="card-body text-center">
                <div class="mb-3">
                  <h5 class="text-dark">Project Main Stack</h5>
                  <div class="row">
                    <div class="col-3 p-2">
                      <img class="w-50" src="{{ asset('img/aws-logo.png') }}" alt="AWS">
                    </div>
                    <div class="col-3 p-2">
                      <img class="w-50" src="{{ asset('img/laravel-logo.png') }}" alt="Laravel">
                    </div>
                    <div class="col-3 p-2">
                      <img class="w-50" src="{{ asset('img/vue-logo.png') }}" alt="Vue.js">
                    </div>
                    <div class="col-3 p-2">
                      <img class="w-50" src="{{ asset('img/pusher-logo.png') }}" alt="Pusher">
                    </div>
                  </div>
                </div>
                <div class="col-12 mt-3 mb-5">
                  <iframe class="width-100" src="{{ asset('img/PusherChat.mp4') }}" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="text-primary attention-shake">
                  <a href="https://bitbucket.org/ismael-gdo/pusher-chat-app" target="_blank">
                    <i class="fab fa-bitbucket"></i> Checkout The Repo
                  </a>
                </div>
              </div>
            </div>

        </div>
    </div>
</div>
@endsection
