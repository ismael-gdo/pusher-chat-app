<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
          'name' => 'Ismael',
          'email' => 'ismael@pusherchatapp.com',
          'password' => bcrypt('S@mpleP@ssword1337'),
          'api_token' => Str::random(60),
          'portfolio' => 'team1',
          'team' => 'kelsea',
          'position' => 'RM'
        ]);

        DB::table('users')->insert([
          'name' => 'Admin',
          'email' => 'admin@pusherchatapp.com',
          'password' => bcrypt('S@mpleP@ssword1337'),
          'api_token' => Str::random(60),
          'portfolio' => 'team1',
          'team' => 'kelsea',
          'position' => 'TL'
        ]);

        DB::table('users')->insert([
          'name' => 'Kelsea',
          'email' => 'kelsea@pusherchatapp.com',
          'password' => bcrypt('S@mpleP@ssword1337'),
          'api_token' => Str::random(60),
          'portfolio' => 'team1',
          'team' => 'kelsea',
          'position' => 'TL'
        ]);

        DB::table('users')->insert([
          'name' => 'Anne',
          'email' => 'anne@pusherchatapp.com',
          'password' => bcrypt('S@mpleP@ssword1337'),
          'api_token' => Str::random(60),
          'portfolio' => 'team2',
          'team' => 'jess',
          'position' => 'RM'
        ]);

        DB::table('users')->insert([
          'name' => 'Nancy',
          'email' => 'nancy@pusherchatapp.com',
          'password' => bcrypt('S@mpleP@ssword1337'),
          'api_token' => Str::random(60),
          'portfolio' => 'team2',
          'team' => 'jess',
          'position' => 'RM'
        ]);
    }
}
